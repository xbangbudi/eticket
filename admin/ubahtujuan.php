<?php
    include_once "header.php";
    require_once '../conn/koneksi.php';
    $id_tempat = $_GET['id'];

    if($_POST){
        $sql = "UPDATE ref_tempat SET nama_tempat='".$_POST['nama']."' WHERE id_tempat=".$id_tempat;
        if ($koneksi->query($sql) === TRUE) {
           echo "<script>
           alert('Data berhasil di update');
           window.location.href='tujuan.php';
           </script>";
        } else {
           echo "Gagal: " . $koneksi->error;
        }
        $koneksi->close(); 
}else{
    $data = mysqli_query($koneksi,"SELECT * FROM ref_tempat WHERE id_tempat=".$id_tempat);
    $d = mysqli_fetch_array($data);
}
?>
<div class="row content-md-center">
     <div class="col-lg-6">
         <form action="" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                 <input type="hidden" value="<?= $d['id_tempat'] ?>" class="form-control" name="id">
                 <label>Nama tempat</label>
                 <input type="text" value="<?= $d['nama_tempat'] ?>" class="form-control" name="nama">
             </div>
             <input type="submit" class="btn btn-primary btn-sm" name="create" value="Ubah">
         </form>
      </div>
</div>
<?php
    include_once "footer.php";
?>