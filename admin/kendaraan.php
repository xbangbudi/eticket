<?php
    include_once "header.php";
?>
<!-- BODY -->
<table class="table table-striped" align="center">
        <tr>
            <td colspan="8"><a class="btn btn-success" href="tambahkendaraan.php">Tambah Kendaraan</a></td>
        </tr>
        <tr>
            <td>No</td>
            <td>Nama Kendaraan</td>
            <td>Jenis Kendaraan</td>
            <td>Biaya</td>
            <td>Tipe Kendaraan</td>
            <td>Foto Kendaraan</td>
            <td>Aksi</td>
        </tr>
        <?php 
		include '../conn/koneksi.php';
		$no = 1;
		$data = mysqli_query($koneksi,"SELECT * FROM ref_kendaraan");
		while($d = mysqli_fetch_array($data)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $d['nama_kendaraan']; ?></td>
				<td><?php echo $d['jenis_kendaraan']; ?></td>
				<td><?php echo "Rp. ".number_format($d['biaya'],0,",","."); ?></td>
				<td><?php echo $d['tipe_kendaraan']; ?></td>
				<td><?= "<img src='../assets/img/kendaraan/".$d['id_gambar']."' width='150' height='100'>" ?></td>
				<td>
                    <a class="btn btn-info btn-block" href="editcus.php?id=<?php echo $d['id_kendaraan']; ?>">EDIT</a>
					<a class="btn btn-danger btn-block" href="hapuscus.php?id=<?php echo $d['id_kendaraan']; ?>">HAPUS</a>
				</td>
			</tr>
			<?php 
		}
		?>
    </table>
<?php
    include_once "footer.php";
?>