<?php
    include_once "header.php";
?>
<!-- BODY -->
<div class="container">

    <table class="table table-striped" align="center">
        <tr>
            <td colspan="8"><a class="btn btn-success" href="tambahtujuan.php">Tambah Tujuan</a></td>
        </tr>
        <tr>
            <td>No</td>
            <td>Nama Tempat</td>
        </tr>
        <?php 
		include '../conn/koneksi.php';
		$no = 1;
		$data = mysqli_query($koneksi,"SELECT * FROM ref_tempat");
		while($d = mysqli_fetch_array($data)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $d['nama_tempat']; ?></td>
				<td>
                    <a class="btn btn-info btn-block" href="ubahtujuan.php?id=<?php echo $d['id_tempat']; ?>">EDIT</a>
					<a onclick="return confirm('Apakah yakin data akan di hapus?')"  class="btn btn-danger btn-block" href="hapustujuan.php?id=<?php echo $d['id_tempat']; ?>">HAPUS</a>
				</td>
			</tr>
			<?php 
		}
		?>
    </table>
</div>
<?php
    include_once "footer.php";
?>