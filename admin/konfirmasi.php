<?php
    include_once "header.php";
?>
<!-- BODY -->
<table class="table table-striped" align="center">
        <tr>
            <td>No</td>
            <td>Nama Pengirim</td>
            <td>Jumlah Pembayaran</td>
            <td>Foto Kendaraan</td>
            <td>Aksi</td>
        </tr>
        <?php 
		include '../conn/koneksi.php';
		$no = 1;
		$data = mysqli_query($koneksi,"SELECT * FROM ta_konfirmasi");
		while($d = mysqli_fetch_array($data)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $d['nama_pengirim']; ?></td>
				<td><?php echo "Rp. ".number_format($d['jumlah_pembayaran'],0,",","."); ?></td>
				<td><?= "<img src='../assets/img/bukti/".$d['bukti_pembayaran']."' width='150' height='100'>" ?></td>
				<td>
                    <a class="btn btn-info btn-block" href="proseskonfirmasi.php?id=<?php echo $d['id']; ?>">KONFIRMASI</a>
				</td>
			</tr>
			<?php 
		}
		?>
    </table>
<?php
    include_once "footer.php";
?>