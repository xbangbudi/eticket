<?php
    include_once "header.php";
?>
<!-- BODY -->
<?php
include '../conn/koneksi.php';
 
// cek apakah yang mengakses halaman ini sudah login
if(isset($_SESSION['username']) == NULL){ 
?>
    <div class="container" style="margin-top:30px">
      <div class="jumbotron">
        <h1 class="display-4 bg-warning">Restricted Area!!</h1>
        <p class="lead">Akses dibatasi, silahkan login terlebih dahulu.</p>
        <hr class="my-4">
        <p>Klik tombol login untuk masuk.</p>
        <a class="btn btn-success btn-lg" href="login.php" role="button">Login</a>
      </div>
    </div>
<?php
}else{
  ?>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Selamat Datang Di Modul Admin</h1>
    <p class="lead">Silahkan Isi Data Master Anda untuk Di Tampilkan Di Halaman Depan</p>
  </div>
</div>
<?php
}
    include_once "footer.php";
?>