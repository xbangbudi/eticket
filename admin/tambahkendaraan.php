<?php
    include_once "header.php";
    require_once '../conn/koneksi.php';
if(isset($_FILES['gambar']['name']) && isset($_FILES['gambar']['tmp_name'])){
    $gambar = $_FILES['gambar']['name'];
    $tmp = $_FILES['gambar']['tmp_name'];
  
// Rename nama fotonya dengan menambahkan tanggal dan jam upload
$gambarbaru = $gambar;
// Set path folder tempat menyimpan fotonya
$path = "../assets/img/kendaraan/".$gambarbaru;
}
if($_POST){
    try {
        if(move_uploaded_file($tmp, $path)){
       $sql = "INSERT INTO ref_kendaraan (id_kendaraan,nama_kendaraan,jenis_kendaraan,biaya,tipe_kendaraan,id_gambar) 
                VALUES ('".$_POST['id']."','".$_POST['nama']."','".$_POST['jenis']."','".$_POST['biaya']."','".$_POST['tipe']."','".$gambarbaru."')";
            }       
       if(!$koneksi->query($sql)){
          echo $koneksi->error;
          die();
        }
    } catch (Exception $e) {
      echo $e;
      die();
    }
    echo "<script>
         window.location.href='kendaraan.php';
         </script>";
}
//count id otomatis
$query = mysqli_query($koneksi,"SELECT max(id_kendaraan) FROM ref_kendaraan");
        $no = mysqli_fetch_array($query);
        if ($no) {
            $nomor = $no[0];
            $kode = (int) $nomor;
            $kode = $kode + 1;
        }else{
            $kode = '1';
        }
?>
<div class="row content-md-center">
     <div class="col-lg-6">
         <form action="" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                 <input type="hidden" value="<?= $kode ?>" class="form-control" name="id">
                 <label>Nama Kendaraan</label>
                 <input type="text" value="" class="form-control" name="nama">
             </div>
             <div class="form-group">
                 <label>Jenis Kendaraan</label>
                 <input type="text" value="" class="form-control" name="jenis">
             </div>
             <div class="form-group">
                 <label>Biaya</label>
                 <input type="text" value="" class="form-control" name="biaya">
             </div>
             <div class="form-group">
                 <label>Tipe Kendaraan</label>
                 <input type="text" value="" class="form-control" name="tipe">
             </div>
             <div class="form-group">
                 <label>Gambar</label>
                 <input type="file" value="" class="form-control" name="gambar">
             </div>
             <input type="submit" class="btn btn-primary btn-sm" name="create" value="Create">
         </form>
      </div>
</div>
<?php
    include_once "footer.php";
?>