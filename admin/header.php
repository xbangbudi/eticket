<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar</title>
</head>
<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/style.css"/>
<script src="../assets/css/bootstrap.min.js"></script>
<script src="../assets/js/jquery-1.11.1.min.js"></script>
<body>
<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">E-TICKET</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <?php 
        // session_start(); 
        session_start();
        if (isset($_SESSION['username']) != NULL){ ?>
        <li class="nav-item active">
          <a class="nav-link" href="kendaraan.php">Tambah Kendaraan<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="tujuan.php">Tambah Tujuan<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="konfirmasi.php">Konfirmasi Pembayaran<span class="sr-only">(current)</span></a>
        </li>
      <?php
        }else{
        ?>
        <li class="nav-item active">
        <a class="nav-link" href="login.php">Login<span class="sr-only">(current)</span></a>
      </li>
      <?php
        }
        ?>
    </ul>
  </div>
</nav>