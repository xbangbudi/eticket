<?php
    include_once "header.php";
?>
<!-- BODY -->
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4" style="margin:10px auto">
            <h1 class="text-center login-title">Silahkan Masuk Terlebih Dahulu</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">
                <form class="form-signin" method="POST" action="proseslogin.php">
                <input type="text" class="form-control" name="user" placeholder="Username" required autofocus>
                <input type="password" class="form-control" name="pass" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Sign in</button>
                </span>
                </form>
            </div>
            <a href="#" class="text-center new-account">Buat Akun</a>
        </div>
    </div>
</div>
<?php
    include_once "footer.php";
?>