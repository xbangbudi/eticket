<?php
    include_once "header.php";
    require_once 'conn/koneksi.php';
    if(isset($_FILES['gambar']['name']) && isset($_FILES['gambar']['tmp_name'])){
        $gambar = $_FILES['gambar']['name'];
        $tmp = $_FILES['gambar']['tmp_name'];
      
    // Rename nama fotonya dengan menambahkan tanggal dan jam upload
    $gambarbaru = $gambar;
    // Set path folder tempat menyimpan fotonya
    $path = "assets/img/bukti/".$gambarbaru;
    }
if($_POST){
    try {
        if(move_uploaded_file($tmp, $path)){
            $sql = "INSERT INTO ta_konfirmasi (nama_pengirim,jumlah_pembayaran,bukti_pembayaran,status) 
                VALUES ('".$_POST['nama']."','".$_POST['jumlah']."','".$gambarbaru."', 0)";
            }       
            if(!$koneksi->query($sql)){
          echo $koneksi->error;
          die();
        }
    } catch (Exception $e) {
      echo $e;
      die();
    }
    echo "<script>
         window.location.href='index.php';
         </script>";
}
?>
<!-- BODY -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h1 style="text-align:center">Isi Data Tujuan Kamu</h1>
        <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <form action="" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                 <input type="hidden" value="<?= $kode ?>" class="form-control" name="id">
                 <label>Nama Pengirim</label>
                 <input type="text" value="" class="form-control" name="nama">
             </div>
             <div class="form-group">
                 <label>Jumlah Pembayaran</label>
                 <input type="text" value="" class="form-control" name="jumlah">
             </div>
             <div class="form-group">
                 <label>Bukti Pembayaran</label>
                 <input type="file" value="" class="form-control" name="gambar">
             </div>
             <input type="submit" class="btn btn-primary btn-sm" name="create" value="Konfirmasi">
         </form>
        </div>
    </div>
</div>
<script>
function hitung() {
    var jumlah = $("#jumlah").val();
    var biaya = $("#biaya").val();
    if (isNaN(jumlah)) 
        {
            alert("Harus Menginput Angka");
            return false;
        }
    total = jumlah * biaya; //a kali b
    $("#total").val(total);
}
</script>
<?php
    include_once "footer.php";
?>
