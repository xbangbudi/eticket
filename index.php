<?php
    include_once "header.php";
?>
<!-- BODY -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h1 style="text-align:center">Pilih Kendaraan Kamu</h1>
        <hr>
        </div>
    </div>
    <div class="row">
        <?php
            require_once 'conn/koneksi.php';
            $data = mysqli_query($koneksi,"SELECT * FROM ref_kendaraan");
            $row = mysqli_num_rows($data);
            if($row > 0){
            foreach($data as $kendaraan){
        ?>
        <div class="col-md-4">
            <div class="card-deck">
                <div class="card">
                <?= "<img src='assets/img/kendaraan/".$kendaraan['id_gambar']."' class='card-img-top' width='223' height='200' name='foto'>" ?>
                    <div class="card-body">
                    <h5 class="card-title"><?= $kendaraan['nama_kendaraan'] ?></h5>
                    <hr>
                    <p class="card-text">Biaya Perjalanan : <?= "Rp. ".number_format($kendaraan['biaya'],0,",","."); ?>/orang</p>
                    </div>
                    <div class="card-footer">
                    <small class="text-muted"><a class="btn btn-info btn-block" href="pesan.php?id=<?php echo $kendaraan['id_kendaraan'];?>" target="_blank">Pesan Tiket</a></small>
                    </div>
                </div>
            </div>
        </div>
            <?php
            }
        }?>
    </div>
</div>
<?php
    include_once "footer.php";
?>