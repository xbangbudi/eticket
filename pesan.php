<?php
    include_once "header.php";
    require_once 'conn/koneksi.php';
    $id_kendaraan = $_GET['id'];
    $kendaraan = mysqli_query($koneksi,"SELECT * FROM ref_kendaraan WHERE id_kendaraan = $id_kendaraan");
    $data = mysqli_fetch_array($kendaraan);
    // var_dump($data);exit;

if($_POST){
    try {
       $sql = "INSERT INTO ta_transaksi (id_kendaraan,id_ticket,asal,tujuan,jumlah_orang,total) 
                VALUES ('".$id_kendaraan."','".$_POST['id_ticket']."','".$_POST['asal']."','".$_POST['tujuan']."','".$_POST['jumlah']."','".$_POST['total']."')";
       if(!$koneksi->query($sql)){
          echo $koneksi->error;
          die();
        }
    } catch (Exception $e) {
      echo $e;
      die();
    }
    header("Location: konfirmasi.php");
}
//count id otomatis
$query = mysqli_query($koneksi,"SELECT max(id_ticket) FROM ta_transaksi");
        $no = mysqli_fetch_array($query);
        if ($no) {
            $nomor = $no[0];
            $kode = (int) $nomor;
            $kode = $kode + 1;
        }else{
            $kode = '1';
        }
?>
<!-- BODY -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h1 style="text-align:center">Isi Data Tujuan Kamu</h1>
        <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <form action="" method="POST">
            <div class="form-row" style="margin: 0px auto">
            <div class="form-group col-md-12">
                <label for="jumlah">Email</label>
                <input type="email" class="form-control" name="mail" placeholder="Email Kamu">
                <input type="hidden" class="form-control" value="<?=$data['biaya']?>" readonly = true name="biaya" id="biaya">
            </div>
                <input type="hidden" class="form-control" name="id_ticket" value="<?=$kode?>" placeholder="Asal Keberangkatan">
                <div class="form-group col-md-6">
                <label for="inputasal">Dari</label>
                <select class="custom-select" id="inputasal" name="asal">
                    <option selected>Pilih Asal Keberangkatan</option>
                    <?php
                        $sql = mysqli_query($koneksi, "SELECT * FROM ref_tempat ORDER BY nama_tempat ASC");
                        if(mysqli_num_rows($sql) != 0){
                            while($row = mysqli_fetch_assoc($sql)){
                                echo '<option>'.$row['nama_tempat'].'</option>';
                            }
                        }
                    ?>
                </select>
                </div>
                <div class="form-group col-md-6">
                <label for="inputtujuan">Tujuan</label>
                <select class="custom-select" id="inputtujuan" name="tujuan">
                    <option selected>Pilih Tujuan Keberangkatan</option>
                    <?php
                        $sql = mysqli_query($koneksi, "SELECT * FROM ref_tempat ORDER BY nama_tempat ASC");
                        if(mysqli_num_rows($sql) != 0){
                            while($row = mysqli_fetch_assoc($sql)){
                                echo '<option>'.$row['nama_tempat'].'</option>';
                            }
                        }
                    ?>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label for="jumlah">Jumlah Penumpang</label>
                <input type="text" class="form-control" onkeyup="hitung()" name="jumlah" id="jumlah" placeholder="Mis: 1">
                <input type="hidden" class="form-control" value="<?=$data['biaya']?>" readonly = true name="biaya" id="biaya">
            </div>
            <div class="form-group">
                <label for="total">Total Perjalanan</label>
                <input type="text" class="form-control" readonly = true name="total" id="total" placeholder="Total">
            </div>
            <button type="submit" class="btn btn-primary">Pesan</button>
        </form>
        </div>
    </div>
</div>
<script>
function hitung() {
    var jumlah = $("#jumlah").val();
    var biaya = $("#biaya").val();
    if (isNaN(jumlah)) 
        {
            alert("Harus Menginput Angka");
            return false;
        }
    total = jumlah * biaya; //a kali b
    $("#total").val(total);
}
</script>
<?php
    include_once "footer.php";
?>
